External Entities BrAPI plugin
******************************

Enables to manage entities from the Breeding API.

===============================

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

This module is a storage client for external entities that can expose BrAPI
objects as Drupal entities. It is built to support any BrAPI object call.

REQUIREMENTS
------------

This module requires the following modules:

 * [External Entities](https://www.drupal.org/project/external_entities)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.
 * If you enabled the module using Drush, you must also rebuild the cache.

CONFIGURATION
-------------

The module has no menu or modifiable global settings. There is no configuration.
When enabled, the module will add a new storage client for external entity
types. Then, when you create a new external entity type, you can select the
"BrAPI" plugin and have access to settings specific to the new external entity.

The "BrAPI Call URL" must be a BrAPI object call with its object identifier
placeholder specified (using the correct BrAPI field name). For instance:
https://www.crop-diversity.org/mgis5/brapi/v2/germplasm/{germplasmDbId}

The module will use the URL with the placeholder stripped to list available
objects. If a corresponding BrAPI "Search call" is provided, it will be used to
load multiple objects at once instead of issuing a call for each object.
The search call can implement both the direct or asynchronous response schemes.
It will be provided as POST data, a JSON filter structure that contains the
placeholder name extracted from the given "BrAPI Call URL" in both singular and
plural form for identifiers to load. Pagination is also supported to retrieve
all objects. The "Search call" can be specified in 2 equivalent ways, using a
fully qualified URL or just the BrAPI call name. For instance, both of these
would work:
search/germplasm
https://www.crop-diversity.org/mgis5/brapi/v2/search/germplasm

It is possbile to ptovide a BrAPI token if the BrAPI endpoint requires one.
Optional query parameters can be specified to pre-filter results.

If a call needs multiples identifiers, it is possible to concatenate them into
a single identifier using "slash" as separator. For instance, to access the
"linkage group name" 456 of the "map" 123 using the configured "BrAPI Call URL"
https://www.crop-diversity.org/mgis5/brapi/v2/maps/{mapDbId}/positions/{linkageGroupName}
the object identifier to use would be "123/456" (instead of just 456).

If no placeholder is specified in a "BrAPI Call URL", the object identifier will
just be appended to the URL and no corresponding search call could be used.

MAINTAINERS
-----------

Current maintainers:
 * Valentin Guignon (guignonv) - https://www.drupal.org/u/guignonv
