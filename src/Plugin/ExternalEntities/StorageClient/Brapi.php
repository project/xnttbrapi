<?php

namespace Drupal\xnttbrapi\Plugin\ExternalEntities\StorageClient;

use Drupal\Core\Form\FormStateInterface;
use Drupal\external_entities\Plugin\ExternalEntities\StorageClient\RestClient;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;

/**
 * External entities storage client based on a REST API.
 *
 * @StorageClient(
 *   id = "brapi",
 *   label = @Translation("BrAPI"),
 *   description = @Translation("Retrieves external entities from a Breeding API enpoint.")
 * )
 */
class Brapi extends RestClient {

  /**
   * Field plural conversion for searching.
   */
  const TO_PLURAL = [
    'attributeCategory' => 'attributeCategories',
    'traitEntity'       => 'traitEntities',
    'entity'            => 'entities',
    'traitClass'        => 'traitClasses',
  ];

  /**
   * Whitelist of fields that should not be pluralized.
   */
  const NO_PLURAL = [
    'page'     => 1,
    'pageSize' => 1,
  ];

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = parent::defaultConfiguration();
    $config['endpoint_options']['id_field'] = '';
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // Change {id} placeholder with the corresponding BrAPI id field set.
    $single_ep = $this->configuration['endpoint_options']['single'] ?? '';
    $id_field = $this->configuration['endpoint_options']['id_field'] ?? '';
    if (!empty($id_field) && !empty($single_ep)) {
      $single_ep = preg_replace(
        '~\{id\}~',
        '{' . $id_field . '}',
        $single_ep
      );
    }

    // Adjust default REST form.
    $form_override = [
      'endpoint' => [
        '#title' => $this->t('BrAPI Call URL'),
        '#description' => $this->t(
          'Enter the BrAPI call URL that returns a list of the requested data type. It may contain query filters or can be a search call. Ex.:<ul><li>"https://www.crop-diversity.org/mgis/brapi/v1/germplasm/?instituteCode=TheInstituteName"</li><li>"https://www.crop-diversity.org/mgis5/brapi/v2/search/germplasm/".</li></ul>'
        ),
        '#attributes' => [
          'placeholder' => $this->t(
            "ex.: https://www.crop-diversity.org/mgis/brapi/v1/germplasm/"
          ),
        ],
        '#required' => FALSE,
      ],
      'endpoint_options' => [
        '#open' => TRUE,
        'cache' => [
          '#type' => 'hidden',
          '#value' => TRUE,
        ],
        'single' => [
          '#required' => TRUE,
          '#title' => $this->t(
            'BrAPI call to load a single data type by its identifier'
          ),
          '#description' => $this->t(
            'Enter the BrAPI call URL to use to fetch a single element. You MUST include curly brackets with the BrAPI identifier field name inside. Ex.:<ul><li>"https://www.crop-diversity.org/mgis/brapi/v1/germplasm/{germplasmDbId}"</li><li>"https://www.crop-diversity.org/mgis5/brapi/v2/germplasm?accessionNumber={accessionNumber}".</li></ul>'
          ),
          '#attributes' => [
            'placeholder' => $this->t(
                "ex.: https://www.crop-diversity.org/mgis/brapi/v2/germplasm/{germplasmDbId}"
            ),
          ],
          '#default_value' => $single_ep,
        ],
        'count' => [
          '#type' => 'hidden',
          '#value' => '',
        ],
        'count_mode' => [
          '#type' => 'hidden',
          '#value' => 'entities',
        ],
      ],
      'response_format' => [
        '#type' => 'hidden',
        '#value' => 'json',
      ],
      'data_path' => [
        '#type' => NULL,
        'list' => [
          '#type' => 'hidden',
          // Works with both 'result' and 'results'.
          '#value' => '$[result,results].data.*',
        ],
        'single' => [
          '#type' => 'hidden',
          '#value' => '$.result',
        ],
        'keyed_by_id' => [
          '#type' => 'hidden',
          '#value' => FALSE,
        ],
        'count' => [
          '#type' => 'hidden',
          '#value' => '$.metadata.pagination.totalCount',
        ],
      ],
      'pager' => [
        '#type' => NULL,
        'page_parameter' => [
          '#type' => 'hidden',
          '#value' => 'page',
        ],
        'page_parameter_type' => [
          '#type' => 'hidden',
          '#value' => 'pagenum',
        ],
        'page_size_parameter' => [
          '#type' => 'hidden',
          '#value' => 'pageSize',
        ],
        'page_size_parameter_type' => [
          '#type' => 'hidden',
          '#value' => 'pagesize',
        ],
        'page_start_one' => [
          '#type' => 'hidden',
          '#value' => FALSE,
        ],
        'always_query' => [
          '#type' => 'hidden',
          '#value' => FALSE,
        ],
      ],
      // Authentication type set in validation if a key is provided.
      'api_key' => [
        'type' => [
          '#type' => 'hidden',
          '#value' => 'none',
        ],
        'key' => [
          '#description' => $this->t(
            'Enter here the BrAPI token to use if the endpoint requires one.'
          ),
          '#title' => $this->t('BrAPI Token'),
        ],
        'header_name' => [
          '#type' => 'hidden',
          '#value' => '',
        ],
      ],
      'http' => [
        '#type' => NULL,
        'headers' => [
          '#type' => 'hidden',
          '#value' => '',
        ],
      ],
      // Add search call body parameters.
      'parameters' => [
        '#title' => $this->t('Optional parameters'),
        '#description' => $this->t(
          'For search calls, optional POST parameters can be specified to pre-filter results. Multiple values can be specified using square brackets and coma-separated values. For instance, use "instituteCode=TheInstituteName" to restrict germplasm to the institute "TheInstituteName" or "species=[SpeciesName1,SpeciesName2]" to restrict to the species "SpeciesName1" or "SpeciesName2".'
        ),
        'list' => [
          '#title' => $this->t('BrAPI search call POST parameters'),
          '#description' => NULL,
        ],
        'list_param_mode' => [
          '#title' => $this->t('BrAPI Call URL uses'),
          '#options' => [
            'query' => $this->t('GET (list calls)'),
            'body' => $this->t('POST (search calls)'),
          ],
          '#description' => NULL,
        ],
        'single' => [
          '#type' => 'hidden',
          '#value' => '',
        ],
        'single_param_mode' => [
          '#type' => 'hidden',
          '#value' => 'query',
        ],
      ],
      // Filtering.
      'filtering' => [
        '#type' => NULL,
        'drupal' => [
          '#type' => 'hidden',
          '#value' => FALSE,
        ],
        'basic' => [
          '#type' => 'hidden',
          // It is partially TRUE. It could be adjusted later if needed.
          '#value' => TRUE,
        ],
        'basic_fields' => [
          '#type' => 'hidden',
          '#value' => '',
        ],
        'list_support' => [
          '#type' => 'hidden',
          '#value' => 'post',
        ],
        'list_join' => [
          '#type' => 'hidden',
          '#value' => ',',
        ],
      ],
    ];

    // Merge forms.
    $form = $this->overrideForm(
      $form,
      $form_override,
      [
        '#type' => 'hidden',
      ]
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function getParametersFormDefaultValue(string $type) :string {
    $default_value = '';

    if (!empty($this->configuration['parameters'][$type])) {
      $lines = [];
      foreach ($this->configuration['parameters'][$type] as $key => $value) {
        if (!empty($value) || (0 === $value) || (FALSE === $value)) {
          if (is_array($value)) {
            $lines[] = "$key=[" . implode(',', $value) . ']';
          }
          else {
            $lines[] = "$key=$value";
          }
        }
        else {
          $lines[] = "$key";
        }
      }
      $default_value = implode("\n", $lines);
    }

    return $default_value;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ) {
    // @todo Make sure single endpoint has curly braces.
    // Enable authentication if API key set.
    $api_key = $form_state->getValue('api_key');
    if (!empty($api_key['key'])) {
      // Make sure key type is properly set before parent validation.
      $api_key['type'] = 'bearer';
      $form_state->setValue('api_key', $api_key);
    }
    parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ) {
    $parameters = $form_state->getValue('parameters') ?? [];
    // Rearrange parameters and handle multiple values.
    foreach (['list', 'single'] as $type) {
      $value = $parameters[$type] ?? '';
      // Make sure the parameters have not been already processed by a child
      // module.
      if (is_string($value)) {
        $lines = explode("\n", $value);
        $lines = array_map('trim', $lines);
        $lines = array_filter($lines, 'strlen');
        $parameters[$type] = [];
        foreach ($lines as $line) {
          $exploded = explode('=', $line);
          if ('' != $exploded[0]) {
            $value = !empty($exploded[1]) ? $exploded[1] : $exploded[0];
            if (('[' == substr($value, 0, 1))
                && (']' == substr($value, -1, 1))
            ) {
              $value = explode(',', substr($value, 1, -1));
            }
            $parameters[$type][$exploded[0]] = $value;
          }
        }
      }
    }
    $form_state->setValue('parameters', $parameters);

    // Save BrAPI data id field name.
    $single_ep = $form_state->getValue(['endpoint_options', 'single']) ?? [];
    if (!empty($single_ep) && preg_match('~\{(\w+)\}~', $single_ep, $matches)) {
      $form_state->setValue(['endpoint_options', 'id_field'], $matches[1]);
      $single_ep = preg_replace('~\{\w+\}~', '{id}', $single_ep);
      $form_state->setValue(['endpoint_options', 'single'], $single_ep);
    }

    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getResponseBody(
    ResponseInterface $response,
    string $request_type,
    string $method,
    string $endpoint,
    array $req_parameters = []
  ) :string {

    // Manage asynchronous searches with "searchResultsDbId".
    if (202 == $response->getStatusCode()) {
      // Asynchronous search, get search token.
      $format = $this->configuration['response_format'];
      $decoder = $this->getResponseDecoderFactory()->getDecoder($format);
      // Force string.
      $body = $response->getBody() . '';
      $body = preg_replace('/^\xEF\xBB\xBF/', '', $body);
      $result = $decoder->decode($body);
      $search_results_db_id =
        $result['result']['searchResultsDbId']
        // If malformed.
        ?? $result['searchResultsDbId']
        ?? '';
      if (empty($search_results_db_id)) {
        $this->logger->error(
          "Failed to use BrAPI search call:\nURL: @endpoint\nMethod: @method\nParameters: @parameters\nResponse body: @body\nError: no searchResultsDbId returned while 202 HTTP response code.",
          [
            '@endpoint' => $endpoint,
            '@method' => $method,
            '@parameters' => print_r($req_parameters, TRUE),
            '@body' => $body,
          ]
        );
      }
      $method = 'GET';
      $endpoint = rtrim($endpoint, '/') . '/' . $search_results_db_id;
      // Add access token and pagging.
      $request = [
        'headers' => $req_parameters['headers'],
        'query' => [],
      ];
      $page = $req_parameters['json']['page'] ?? NULL;
      if (isset($page)) {
        $request['query']['page'] = $page;
      }
      $page_size = $req_parameters['json']['pageSize'] ?? NULL;
      if (isset($page_size)) {
        $request['query']['pageSize'] = $page_size;
      }
      // Try for 5 min max.
      $timeout = time() + 300;
      $trials = 0;
      // Start to query every 1 sec.
      $sleep_time = 1;
      while ((time() < $timeout)
        && (202 == $response->getStatusCode())
      ) {
        // Wait before re-querying.
        sleep($sleep_time);
        switch (++$trials) {
          case 10:
            // Switch to 2sec delay after 10sec.
            $sleep_time = 2;
            break;

          case 15:
            // Switch to 5sec delay after 20sec.
            $sleep_time = 5;
            break;

          case 29:
            // Switch to 10sec delay after 1min30.
            $sleep_time = 10;
            break;
        }
        // Manage delays between queries.
        $this->ensureQueryLimits();
        try {
          $response = $this->httpClient->request(
            $method,
            $endpoint,
            $request
          );
        }
        catch (GuzzleException $e) {
          $this->logger->error(
            "Failed to use BrAPI search call:\nURL: @endpoint\nMethod: @method\nParameters: @parameters\n@exception",
            [
              '@endpoint' => $endpoint,
              '@method' => $method,
              '@parameters' => print_r($req_parameters, TRUE),
              '@exception' => '' . $e,
            ]
          );
          return '';
        }
      }
    }

    if ($response && (200 == $response->getStatusCode())) {
      // Force string.
      $body = $response->getBody() . '';
      // Remove UTF8 BOM added by some server which may intefer with data
      // parsers.
      $body = preg_replace('/^\xEF\xBB\xBF/', '', $body);
    }
    else {
      // Don't know how to handle non-200 answers: return an empty string.
      $body = '';
    }

    return $body;
  }

  /**
   * {@inheritdoc}
   */
  public function transliterateDrupalFilters(
    array $parameters,
    array $context = []
  ) :array {
    $transliterated_filters = parent::transliterateDrupalFilters(
      $parameters,
      $context
    );
    // For search calls, we need to pluralize field names.
    $endpoint = $this->getEndpointUrl('list');
    $method = $this->getRequestMethod('list', $endpoint);
    // We could check using a regexp 'brapi/v\d/search' but we can assume no
    // site would have the string '/search' before a brapi service.
    if (('POST' == $method) && (FALSE !== strrpos($endpoint, '/search'))) {
      if (!empty($transliterated_filters)) {
        foreach ($transliterated_filters['source'] as $index => $filter) {
          if (!empty($filter['field'])
            && empty(static::NO_PLURAL[$filter['field']])
          ) {
            $field_name = $filter['field'];
            // Pluralize field name.
            if (array_key_exists($field_name, static::TO_PLURAL)) {
              $field_name = static::TO_PLURAL[$field_name];
            }
            else {
              $field_name .= 's';
            }
            $transliterated_filters['source'][$index]['field'] = $field_name;
            // Make sure values are in an array.
            if (!is_array($filter['value'])) {
              $transliterated_filters['source'][$index]['value'] = [$filter['value']];
            }
          }
        }
      }
    }
    return $transliterated_filters;
  }

}
